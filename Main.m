clc
% close all
clear all

n_gaussian = 5;     % Number of Guassian base functions for smoothed-time controller 

%% PI2 Model Parameter
PI2_Model_Param = struct;
PI2_Model_Param.alpha = pi/4;                           % Anordnungswinkel der Omniwheels
PI2_Model_Param.m_K = (2.29)*1.1;                             % Masse der Kugel
PI2_Model_Param.m_AW = (16+1.18)*1.1;                         % Masse des Aufbaus und der Motoren und Omniwheels mit H�lle ohne Kopf
PI2_Model_Param.m_tot = PI2_Model_Param.m_AW+PI2_Model_Param.m_K;% Totale Masse
PI2_Model_Param.r_K = 0.125*1.1;                            % Radius der Kugel
PI2_Model_Param.r_W = 0.06;                             % Radius der Omniwheels
PI2_Model_Param.l = 0.366;                              % Hoehe des Schwerpunktes ab Kugelmittelpunkt
PI2_Model_Param.Theta_K_i = 2/3*PI2_Model_Param.m_K*(PI2_Model_Param.r_K-0.004)^2;      % Traegheit der Kugel
PI2_Model_Param.Theta_KW_i = 2/3*PI2_Model_Param.m_K*PI2_Model_Param.r_K^2;             % Traegheit der Kugel und der Omniwheels mit �bersetzung (Trick!)
PI2_Model_Param.A_Theta_AW_x = 1.201+PI2_Model_Param.m_AW*PI2_Model_Param.l^2+0.878*(1/12*0.7^2+1/4*0.1^2);  % Traegheiten des Koerpers um Schwerpunkt plus Steiner
PI2_Model_Param.A_Theta_AW_y = 1.199+PI2_Model_Param.m_AW*PI2_Model_Param.l^2+0.878*(1/12*0.7^2+1/4*0.1^2);
PI2_Model_Param.A_Theta_AW_z = 0.217+1/2*0.878*0.1^2;
PI2_Model_Param.Theta_OW = 900e-6;                      % Traegheit eines Omniwheels
PI2_Model_Param.Theta_M = 3.33e-6;                      % Traegheit eines Motors
PI2_Model_Param.i_gear = 26;                            % Untersetzung des Getriebes
PI2_Model_Param.Theta_W_i = PI2_Model_Param.Theta_OW+PI2_Model_Param.i_gear^2*PI2_Model_Param.Theta_M;  % Traegheit der Motoren und Omniwheels
PI2_Model_Param.g = 9.81;                               % Gravitationskonstante
PI2_Model_Param.max_motor_torque = 4;

% save PI2_Model_Param PI2_Model_Param

%% PI2 Task structure
PI2_Task = struct;
PI2_Task.dt         = 0.02;
PI2_Task.start_x    = rand(10,1)*0.1-0.05;%zeros(10,1);
PI2_Task.start_time = 0;
PI2_Task.goal_pos   = [3 -1.8]';
PI2_Task.goal_vel   = [0 0]';
PI2_Task.goal_time  = 10;
PI2_Task.max_iteration = 50;
PI2_Task.num_rollouts  = 10;
PI2_Task.num_reuse     = 9;
% PI2_Task.random     = randn(11*n_gaussian,PI2_Task.goal_time/PI2_Task.dt + 1,300);
PI2_Task.random     = repmat(randn(11*n_gaussian,1,300),[1 PI2_Task.goal_time/PI2_Task.dt+1 1]);

x_sym = sym(zeros(10,1));
u_sym = sym(zeros(3,1));
for cnt = 1:10
    x_sym(cnt)  = sym(sprintf('x%d',cnt),'real');
end
for cnt = 1:3
    u_sym(cnt)  = sym(sprintf('u%d',cnt),'real');
end
% h = 100 *( (-0.125*x_sym(7)-PI2_Task.goal_pos(2))^2 + (0.125*x_sym(9)-PI2_Task.goal_pos(1))^2 ) + ...
%     40*( (-0.125*x_sym(8)-PI2_Task.goal_vel(2))^2 + (0.125*x_sym(10)-PI2_Task.goal_vel(1))^2 ) + ...
%     x_sym'*diag([40 4 40 4 40 4 0 0 0 0])*x_sym;
h = 100 *( (-0.125*x_sym(7)-PI2_Task.goal_pos(2))^2 + (0.125*x_sym(9)-PI2_Task.goal_pos(1))^2 ) + ...
    40*( (-0.125*x_sym(8)-PI2_Task.goal_vel(2))^2 + (0.125*x_sym(10)-PI2_Task.goal_vel(1))^2 ) + ...
    x_sym'*diag([40 4 40 4 40 4 0 0 0 0])*x_sym;
l = 10*x_sym(5)^2 + 1*x_sym(6)^2 + 0.1*(u_sym'*u_sym);

Cost   = struct;
Cost.x = x_sym;
Cost.u = u_sym;
Cost.h = h;
Cost.l = l;
PI2_Task.cost = Cost;

% save PI2_Task PI2_Task 

%%
% Define the reward function
temp1 = PI2_Task.cost.h;
qf_fun  = matlabFunction(temp1,'vars',{PI2_Task.cost.x});

temp2 = PI2_Task.cost.l*PI2_Task.dt;
q_fun   = matlabFunction(temp2,'vars',{PI2_Task.cost.x,PI2_Task.cost.u});

% Transform Controller structure from ILQG convention to PI2
load ILQG_Controller 
% load LQR_Controller 
Controller = BaseFcnTrans(ILQG_Controller,n_gaussian);

%% PI2 Algorithm for adaptation
t_cpu = cputime;

AllCost = zeros(PI2_Task.max_iteration+1,1);
AllController(1:PI2_Task.max_iteration+1,1) = Controller;
Allgoods = 1;
num_time_steps = (PI2_Task.goal_time-PI2_Task.start_time)/PI2_Task.dt + 1;

bsim_out = [];
bR = zeros(PI2_Task.num_rollouts,num_time_steps);

for cnt1 = 1:PI2_Task.max_iteration+1
    
    % Modify the noise covariance through noise annealing multiplier
    noise_mult = double(PI2_Task.max_iteration+1 - cnt1)/double(PI2_Task.max_iteration);
    noise_mult = max([0.005 noise_mult*0.01]);
    Task = PI2_Task;
    Task.random = noise_mult * Task.random;
        
    % Perform a test rollout to evaluate the current solution
    test_Task = PI2_Task;
    test_Task.random = 0.005 * test_Task.random;
    Test_sim_out  = Ballbot_Simulator_protected(PI2_Model_Param,test_Task,Controller);
    
    % computing the displacement of the ball center
    temp = odometryIntegration(PI2_Model_Param,Test_sim_out);
        
    h = 100 *( (temp.odometry(3,end)-PI2_Task.goal_pos(2))^2 + (temp.odometry(1,end)-PI2_Task.goal_pos(1))^2 ) + ...
        40*( (temp.odometry(4,end)-PI2_Task.goal_vel(2))^2 + (temp.odometry(2,end)-PI2_Task.goal_vel(1))^2 ) + ...
        Test_sim_out.x(:,end)'*diag([40 4 40 4 40 4 0 0 0 0])*Test_sim_out.x(:,end);
    
    figure(11)
    hold on
    plot(temp.odometry(1,:),temp.odometry(3,:),'.')
    hold off
    drawnow
    
    % Save the cost of test rollout
    temp = [q_fun(Test_sim_out.x(:,1:end-1),Test_sim_out.u(:,1:end-1))...
                h];
    AllCost(cnt1) = sum(temp);
    
    % Print some information out
    fprintf('Cost %2d: %6.4f \n',cnt1-1,AllCost(cnt1))
       
%     % Block bad solution and go back randomly to the one of the 5-recent
%     % acceptable solutions
%     if cnt1 > 1
%         if AllCost(cnt1-1)*1.1 < AllCost(cnt1) || isnan(AllCost(cnt1))
%             temp = randperm(5);  temp = temp(1)-1;
%             temp = max(1,length(Allgoods)-temp);
%             Controller = AllController(Allgoods(temp));
%             fprintf('Iteration %d is blocked back to %d \n',cnt1-1,Allgoods(temp))
%         else
%             Allgoods = [Allgoods; cnt1];
%         end
%     end
%     
%     if cnt1 > Task.max_iteration
%         break;
%     end
    
    % Create "Task.num_rollouts" rollouts and save the information in
    % "bsim_out" and "bR"
    for cnt2 = length(bsim_out)+1:Task.num_rollouts
        
        % Simulate a rollout
        sim_out  = Ballbot_Simulator_protected(PI2_Model_Param,Task,Controller);
        bsim_out = [bsim_out; sim_out];
        
        temp = odometryIntegration(PI2_Model_Param,sim_out);
        x_temp(cnt2,1:length(sim_out.t)) = temp.odometry(1,:);
        y_temp(cnt2,1:length(sim_out.t)) = temp.odometry(3,:);
        
        h = 100 *( (temp.odometry(3,end)-PI2_Task.goal_pos(2))^2 + (temp.odometry(1,end)-PI2_Task.goal_pos(1))^2 ) + ...
            40*( (temp.odometry(4,end)-PI2_Task.goal_vel(2))^2 + (temp.odometry(2,end)-PI2_Task.goal_vel(1))^2 ) + ...
            sim_out.x(:,end)'*diag([40 4 40 4 40 4 0 0 0 0])*sim_out.x(:,end);
        
        % Calculate the reward for each time step
        temp1 = q_fun(sim_out.x(:,1:end-1),zeros(size( sim_out.u(:,1:end-1)) ));
        temp2 = h;
        bR(cnt2,:) = [temp1 repmat(temp2,1,num_time_steps-length(temp1))];
    end
    
    figure(11)
    clf
    plot(x_temp',y_temp',PI2_Task.goal_pos(1),PI2_Task.goal_pos(2),'*')
    drawnow
    
    % PI2_Algorithm
    [g,sigma] = PI2_01(bsim_out,bR);
    
    % Update parameters
    Controller.theta = Controller.theta + g;
    
    % Save Controller
    AllController(cnt1+1) = Controller;
    
    %% 
    % Reuse the 'num_reuse'-best trials and re-evalute them for the
    % next update in the spirit of importance sampling
    [~,index] = sort(sum(bR,2),'ascend');
    fprintf('The 3-best rollouts in iteration %d: %d %d %d \n',cnt1,index(1),index(2),index(3))
    index = index(1:Task.num_reuse);
    bR = bR(index,:);
    bsim_out = bsim_out(index);
    % Substituting old Controller with new Controller
    for cnt = 1:length(bsim_out)
        bsim_out(cnt).Controller.theta = Controller.theta;
        temp = repmat(g,[1 1 size(bsim_out(cnt).eps,3)]);
        bsim_out(cnt).eps = bsim_out(cnt).eps - temp;
    end
    

end

t_cpu = cputime - t_cpu;
fprintf('CPU time: %f \n',t_cpu);

%% Final test simulation
sim_out = Ballbot_Simulator_protected(PI2_Model_Param,Task,Controller);
final_pose = [0.125*sim_out.x(9,end) -0.125*sim_out.x(7,end)]';
disp(['Center of the ball Final Pose: [' num2str(final_pose(1)) '; ' ...
    num2str(final_pose(2)) ']'])

%% Plots 
% Plot ballbot states and control inputs
myplots(sim_out)
% Plot learning curve
figure(4)
plot(0:length(AllCost)-1,AllCost)
xlabel('Iteration');  ylabel('Cost');  grid on


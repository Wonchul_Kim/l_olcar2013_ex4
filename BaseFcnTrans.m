function new_Controller = BaseFcnTrans(Controller,numG)
% 
% Controller: The Controller structure defined in the ILQG convention
% numG: Number of Gaussian base-functions
% new_Controller: The Controller structure defined in the eNAC convention

new_Controller = Controller;
new_Controller.theta   = zeros(11*numG,3,1);
new_Controller.BaseFnc = @BaseFnc;
new_Controller.time    = [];

Ap = BaseFnc(Controller.time,ones(10,length(Controller.time)))';
Ap = Ap(:,1:end-1); 
A = [];
n = length(Controller.time);
for cnt = 1:n
    temp = Ap( cnt, : );
    temp1 = repmat(eye(11),[1 numG])*diag(temp);
    A = [A; temp1];
end
    
B1 = squeeze( Controller.theta(:,1,:));  B1 = B1(:);
B2 = squeeze( Controller.theta(:,2,:));  B2 = B2(:);
B3 = squeeze( Controller.theta(:,3,:));  B3 = B3(:);
B = [B1 B2 B3];
temp = (A'*A+0.001*eye(numG*11))\(A'*B);
new_Controller.theta = [temp; 0.03*ones(1,3)];

%% Base function definition
function f = BaseFnc(t,x)
    
    delta   = Controller.time(end)/(numG-1);
    centers = (0:delta:Controller.time(end))';
    sigma2  = delta^2/(8*log(1.8));
    
    f = zeros(11*numG,length(t));
    
    for cnt0 = 1:length(t)
                
        temp = t(cnt0) - centers;
        firing = exp(-temp.^2/(2*sigma2)) / sqrt(2*pi*sigma2);
        
        local_lin = [1; x(:,cnt0)] * firing';
        
        f(:,cnt0) = local_lin(:);
    end
    
    f = [f; zeros(1,length(t))];

end


end


function sim_out = odometry(Model_Param,sim_out)

tspan = sim_out.t;
Y = zeros(5,length(sim_out.t));

x0 = zeros(5,1);
options = odeset('RelTol',1e-5,'AbsTol',1e-4);
[~,X]   = ode45(@OdometryEquation,tspan,x0,options);

for cnt = 1:length(sim_out.t)
    Y(:,cnt) = odometry_fnc (Model_Param,sim_out.x(:,cnt));
end
    
X = X';    
sim_out.odometry = [X(1,:); Y(1,:); X(2,:); Y(2,:); X(3,:);...
    Y(3,:); X(4,:); Y(4,:); X(5,:); Y(5,:); ];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Nested ballbot equation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function dx = OdometryEquation(t,x)
    
    index  = sum(sim_out.t <= t);
    dx   = odometry_fnc (Model_Param,sim_out.x(:,index));
  
end

end



function [odometry_data] = odometry_fnc (Model_Param,states)
% odometry_data = [r_P_dot_x, r_P_dot_y, psi_dot_1, psi_dot_2, psi_dot_3]
% states = [theta_x, theta_dot_x, theta_y, theta_dot_y,
% theta_z, theta_dot_z, phi_x, phi_dot_x, phi_y, phi_dot_y]

r_K = Model_Param.r_K;
r_W = Model_Param.r_W;


%% Demux

theta_x = states(1);
theta_dot_x = states(2);
theta_y = states(3);
theta_dot_y = states(4);
theta_z = states(5);
theta_dot_z = states(6);
phi_x = states(7);
phi_dot_x = states(8);
phi_y = states(9);
phi_dot_y = states(10);

%% Translational speed of center of ball P in intertial System I

r_P_dot_x = r_K*(sin(theta_z)*phi_dot_x+cos(theta_z)*phi_dot_y);
r_P_dot_y = r_K*(sin(theta_z)*phi_dot_y-cos(theta_z)*phi_dot_x);


%% Rotational speed of the omniwheels in A:

psi_dot_1 = r_K/(sqrt(2)*r_W)*((-cos(theta_y)+cos(theta_x)*sin(theta_y))*phi_dot_x+theta_dot_x+sin(theta_x)*(-phi_dot_y+theta_dot_y)-(cos(theta_x)*cos(theta_y)+sin(theta_y))*theta_dot_z);
psi_dot_2 = r_K/(2*sqrt(2)*r_W)*((cos(theta_y)+(2*cos(theta_x)-sqrt(3)*sin(theta_x))*sin(theta_y))*phi_dot_x-theta_dot_x-(sqrt(3)*cos(theta_x)+2*sin(theta_x))*(phi_dot_y-theta_dot_y)+cos(theta_y)*(-2*cos(theta_x)+sqrt(3)*sin(theta_x))*theta_dot_z+sin(theta_y)*theta_dot_z);
psi_dot_3 = r_K/(2*sqrt(2)*r_W)*((cos(theta_y)+(2*cos(theta_x)+sqrt(3)*sin(theta_x))*sin(theta_y))*phi_dot_x-theta_dot_x+(sqrt(3)*cos(theta_x)-2*sin(theta_x))*(phi_dot_y-theta_dot_y)-cos(theta_y)*(2*cos(theta_x)+sqrt(3)*sin(theta_x))*theta_dot_z+sin(theta_y)*theta_dot_z);

%% Mux
odometry_data = zeros(5,1);
odometry_data(1) = r_P_dot_x;
odometry_data(2) = r_P_dot_y;
odometry_data(3) = psi_dot_1;
odometry_data(4) = psi_dot_2;
odometry_data(5) = psi_dot_3;

end